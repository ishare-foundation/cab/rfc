# Deep dive Meetings

{% hint style="info" %}
If you would like to be a part of one or more RFC deep dive meetings, please [reach out to us](https://ishare.eu/home/contact/).
{% endhint %}

About the deep dive meetings:

* Discuss one or two RFCs
* Focus on requirements and impact analysis
* Prepare RFCs for CAB
* Weekly on Tuesday afternoon
* Participation open to all

Outcome of the deep dive meetings is added to the discussed [RFC issues on Gitlab](https://gitlab.com/ishare-foundation/cab/rfc/-/boards).
