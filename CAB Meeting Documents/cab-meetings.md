# CAB Meetings

The Change Advisory Board meetings are part of the [iSHARE Governance](https://framework.ishare.eu/is/governance-framework):

* The Change Advisory Board consists of subject matter experts (legal/ operational/ functional/ technical) delegated by the participants and data spaces.
* The Change Advisory Board advises the Scheme Owner on changes to the specifications of the iSHARE Trust Framework. 

About the CAB Meetings:

* Discuss proposed RFCs
* Advice on proposed RFCs
* Proceed with impact analysis or reject
* Advice on impacted RFCs
* Proceed with implementation or reject
* Quarterly on Wednesday afternoon