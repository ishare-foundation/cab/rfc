# 2025-04-02 (extraordinary CAB)

This CAB still needs to be scheduled. The CAB has special focus on two impact analysis that have been drafted for the next release.

### Agenda

1. Welcome + introduce new participants
2. Advice on the implementation of RFCs
   1. [Tentative: RFC037: Redefine licenses, add licenses and make them machine readable](https://gitlab.com/ishare-foundation/cab/rfc/-/issues/7)
   2. [Tentative: RFC061: Add CRUD operations to Parties endpoint](https://gitlab.com/ishare-foundation/cab/rfc/-/issues/30)
3. Should a version 2.2 be released with these RFCs or should they be included in version 3.0?
4. Release 3.0
   1. RFC044 and RFC049 have been added to the release
