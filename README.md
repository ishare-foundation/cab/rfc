---
layout:
  title:
    visible: true
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: false
  pagination:
    visible: true
---

# iSHARE Change Management

Welcome to the documentation of iSHARE's Change Management process.

* For more information regarding iSHARE, please refer to iSHARE's [website](https://www.ishare.eu)
* For more information about the change management process as part of the [iSHARE Trust Framework](https://framework.ishare.eu) refer to [this page](https://framework.ishare.eu/is/change-management)

The change management process is the responsibility of the Scheme Owner (iSHARE Foundation), working together with the Change Advisory Board.

Please review the pages about the [RFC process](<Change Management Process.md>) and the [impact analysis procedure](<Impact Analysis Procedure.md>) to understand more about the process.

All documentation about the organised meetings (CAB and deep dive meetings) is kept in [CAB Meeting Documents & Schedule](https://changes.ishare.eu/v/cab-and-deepdive-meetings). Impact analysis of RFCs are kept [here](https://changes.ishare.eu/v/rfc-impact-analysis-1). A complete overview of all RFCs, their status and priority, is kept is as [Gitlab issue board](https://gitlab.com/ishare-foundation/cab/rfc/-/boards).
