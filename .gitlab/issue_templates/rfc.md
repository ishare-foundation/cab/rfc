> **_NOTE:_**  Do not assign and RFC number yourself. If you would like to raise an issue that could lead to an RFC, raise the issue without an RFC number. The Change Manager will discuss the proposed RFC in the Change Advisory Board and assign an RFC number if the change is accepted.

## Background and rationale

Describe the current situation and the complications that have led to the creation of this RFC.

## Proposed change: purpose

Describe the purpose of the RFC. What will it solve and what use cases will benefit from the change.

## Proposed change: considerations and requirements

Describe all relevant considerations and requirements that should be taken into account when performing the impact analysis on the RFC.