# RFCXXX: <Name>

## Background and rationale

Describe the current situation and the complications that have led to the creation of this RFC.

## Proposed change

### Purpose

Describe the purpose of this RFC: why is it created?

### Description and principles

Describe the requested changes and list general principles that were taken into account when drafting this RFC.

If the change impacts the [iSHARE Trust Framework](https://framework.ishare.eu) or the [developer documentation](https://dev.ishare.eu), make sure to describe the changes using branches and merge requests on the existing repositories as much as possible.

### Example use cases

Clarify the RFC by describing example use cases in which the change is used in practice.

## Impact on the ecosystem

The following table lists the impact of this RFC on the formal iSHARE roles (excluding the Scheme Owner role).

| Formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Service Consumer          | Yes/no            | Yes/no                                             |
| Service Provider          | Yes/no            | Yes/no                                             |
| Entitled party            | Yes/no            | Yes/no                                             |
| Authorization Registry    | Yes/no            | Yes/no                                             |
| Identity Provider         | Yes/no            | Yes/no                                             |
| Identity Broker           | Yes/no            | Yes/no                                             |
| Data Space Administrator  | Yes/no            | Yes/no                                             |
| Satellite                 | Yes/no            | Yes/no                                             |

## Impact iSHARE Foundation (Scheme Owner)

- The [iSHARE Trust Framework](https://framework.ishare.eu)
- The [developer documentation](https://dev.ishare.eu) (as an extension of the iSHARE Trust Framework)
- The OpenAPI definitions on [Swaggerhub](https://app.swaggerhub.com/search?owner=iSHARE)
- Example implementation in [Postman Collections](https://dev.ishare.eu/demo-and-testing/postman.html)
- Code that is published on Github:
    - [iSHARE Satellite reference implementation](https://github.com/iSHAREScheme/iSHARESatellite)
    - [eSEAL certificate procurement guide](https://github.com/iSHAREScheme/eSEALsGuide)
    - [iSHARE.NET service consumer core components](https://github.com/iSHAREScheme/iSHARE.NET)
    - [Python iSHARE package](https://github.com/iSHAREScheme/python-ishare)
    - [iSHARE code snippets](https://github.com/iSHAREScheme/code-snippets)
    - [Reference implementation for Authorization Registry](https://github.com/iSHAREScheme/AuthorizationRegistry)
    - [Reference implementation for Service Provider](https://github.com/iSHAREScheme/ServiceProvider)
- The implementation of the iSHARE satellite for iSHARE as the Scheme Owner on https://sat.ishare.eu and https://sat.uat.isharetest.net
- The [public website](https://www.ishare.eu)
- Internal documentation
- [Authorization Registry test implementation](https://ar.isharetest.net/)
- The [Conformance Test Tool](https://ctt.isharetest.net/admin/account/login), tests listed on https://ctt.isharetest.net/admin/test-cases
- iSHARE test satellite (used for conformance testing): https://scheme.isharetest.net/
- iSHARE test certificate authority: [EJBCA Public Web](https://ca7.isharetest.net:8442/ejbca/)
- [iSHARE Change Management documentation](https://changes.ishare.eu)


## Implementation

### Release schedule

Describe the foreseen release planning for this RFC. Should it be combined with other RFCs? What is the required release timeframe?

### Communication

Describe required communication topics and means.
