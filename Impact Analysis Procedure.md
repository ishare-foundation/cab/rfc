# Impact Analysis Procedure

This document describes how impact analysis is considered a collaborative effort within the iSHARE Change Management process and how this is implemented using Gitlab.

## Start of impact analysis

After an RFC has been discussed in the Change Advisory Board and approved by the Change Manager, work on impact analysis can start. The RFC is given an RFC number by the Change Manager and moved on the [board](https://gitlab.com/ishare-foundation/cab/rfc/-/boards) from "RFC Identified" to "RFC Impact Analysis" (or given the corresponding labels).

## Using Gitlab to collaborate on RFCs

If you are familiar with using Gitlab: the general approach is to fork the project, commit to the fork and then do a merge request to the upstream repository. In the steps below, this procedure is explained.

### 1. Fork the project

- Make sure you have a Gitlab account and login into Gitlab
- Go to the projects [fork page](https://gitlab.com/ishare-foundation/cab/rfc/-/forks)
- Click on "Fork" (upper right) to fork the project
- Execute the steps and create your fork

### 2. Editing and committing

Use one of the the following methods for editing. After editing you need to *commit* changes to you fork. This can be considered as saving. In order to prevent conflicts, it's advisable to choose one of the following methods and stick to it.

#### Local IDE

Using a local IDE (for instance Visual Studio Code). This requires a local install of Git and some knowledge of Git. Commit using your local Git (or as integrated in the IDE).

#### Online IDE

Using the online IDE provided by Gitlab. [This image](/Assets/WebIDE.png) shows where to open the online IDE. After editing commit your changes as shown in [this image](/Assets/Commit%20in%20WebIDE.png).

#### Single file

For small changes, consider editing the single file. [This image](/Assets/Single%20file%20edit.png) shows how to edit a single file. After you are done editing, press the Commit button to save your changes.

### 3. Propose your changes by a merge request

When you are done editing and committing your contribution to the RFC impact analysis, you can request the Change Manager to review and accept your changes. This is done by doing a Gitlab *merge request* (in Github this is called a *pull request*).

- Open your fork on Gitlab.com
- In the menu, under "Code", click on "Merge requests"
- Click on the button "New merge request"
- Make sure that:
-- On the left side under Source branch your fork is selected, with the branche you have been working on (usually "main")
-- On the right side under Target branche ishare-foundation/cab/rfc, branch main is selected
- Click "Compare branches and continue"
- Describe the changes you have made using the title and description
- Click on "Create merge request"

The changes you have made are then propsoed to the Change Manager and integrated in the RFC if they are accepted. 

## Suggested file structure of an RFC folder

When writing an RFC, please conform to the following rules:

- All documents regarding an RFC should be stored in a subfolder in [/RFC Documents](/RFC%20Documents/) named RFC###, where ### is the number of the RFC that was assigned to the RFC by the change manager
- Write the RFC by copying the [RFC Template.md](/RFC%20Template.md) to the RFC folder, renaming it to **index.md**