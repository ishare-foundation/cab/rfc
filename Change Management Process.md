# RFC Process

## Scope of the change management process

This change management process is designed to be used for changes that impact the [iSHARE Trust Framework](https://framework.ishare.eu). Trivial changes (such as typo's or the addition of extra clarifications) are out of scope of this process.

## Change management process and RFCs

The change management process is designed around Request For Changes (RFCs). The following diagram describes the change management process.

![Change management process diagram](<Assets/Change Management Process.png>)

If any required additional changes arise after implementing an RFC, a new RFC must be raised. An RFC cannot be _reopened_.

### Roles in the change management proces

The following roles are recognised in the change management process:

* Participant: any party involved in iSHARE as a participant, as defined in [Framework and Roles](https://framework.ishare.eu/is/framework-and-roles)
* Change Manager: the person responsible on behalf of the Scheme Owner for managing changes. The Change Manager:
  * Maintains the [RFC backlog](https://gitlab.com/ishare-foundation/cab/rfc/-/boards)
  * Maintains the [RFC template](RFC%20Template.md)
  * Facilitates the process
  * Communicates the process outcomes
* Change Advisory Board (CAB): a group that is open to (potential) iSHARE participants, which advises on RFC's.
* Change Advisory Board chair: the chair of the Change Advisory Board. The chair responsible for organising CAB meetings.
* Executive Board: responsible for handling first line escalations (see [escalation procedure](<Change Management Process.md#escalation-procedure>))
* Council of Participants: responsible for handling second line escalations (see [escalation procedure](<Change Management Process.md#escalation-procedure>))

### Escalation procedure

If one of the parties involved in the process is unsatisfied with the process's outcome, the following escalation path should be followed:

1. Escalate to the Executive Board of iSHARE Foundation (acting as Scheme Owner).
2. Escalate to the Council of Participants of the iSHARE Foundation.

## The change management process as implemented using Gitlab

Various features of Gitlab support the change management process, as described below using the phases of the change management process.

### Proposing a change

Any (potential) participant can propose a required change. The proposed change must be registered in Gitlab, using a [Gitlab issue](https://gitlab.com/ishare-foundation/cab/rfc/-/issues/new). The RFC Template should be used as a reference for providing the required information. The RFC must be labelled "RFC Proposed".

The Change Manager will evaluate the requested change. If the requirements are not clear enough to start an impact analysis, the RFC will be labelled "RFC requirements definition".

### Requirements definition

Before an impact analysis can be performed, it is vital to have a solid understanding of the requirements that have led to the RFC. This is usually a process in which iSHARE can participate, but will not lead. If the requirements are clear, the Change Manager can discuss the RFC in the Change Advisory Board and assign an RFC number to it, or reject it. After assigning an RFC number the RFC will be labelled "RFC Impact Analysis" or "RFC Rejected".

### Impact analysis

Next step in the change management process is performing an impact analysis. This will result in a more detailed description, possibly combined with other assets in a subfolder per RFC in the [/RFC Documents](RFC%20Documents/) folder. Impact Analysis is considered a collaborative effort, in which each (potential) iSHARE participant is invited to take part in. For more information on contributing to RFC impact analysis, please refer to the \[Contribution procedure]\(/Impact Analysis Procedure.md).

After performing impact analysis, the RFC will be discussed (again) in the Change Advisory Board. The Change Advisory Board will advice on the implementation or rejection of the RFC, after which the Change Manager will make a formal decision to implement the RFC or reject it. The RFC will then be labelled "RFC Approved and ready for implementation" or "RFC Rejected".

### Implementation

When implementation is started, the RFC will be labelled "RFC implementation" and remain so until implementation is finished, after which the RFC will be labelled "RFC Implemented".

## Gitlab repository

This respository should be considered the single source of truth, where all latest RFCs are published. The following file and directory structure is maintained:

| File/directory                                                 | Remark                                                                                          |
| -------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| [/Assets](Assets/)                                             | Support folder containing general assets                                                        |
| [/CAB Meeting Documents](CAB%20Meeting%20Documents/)           | Contains preperations for CAB meetings and meeting notes                                        |
| [/RFC Documents](RFC%20Documents/)                             | Contains subfolder per RFC in which all relevant RFC documents and assets are stored per folder |
| [Change Management Process.md](<Change Management Process.md>) | This file                                                                                       |
| [RFC Template.md](RFC%20Template.md)                           | Holds the RFC Template and should only be edited by the change manager                          |

The RFC backlog is managed using [Gitlab Issues](https://gitlab.com/ishare-foundation/cab/rfc/-/issues).
