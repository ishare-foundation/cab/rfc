# RFC051: Revert to standard OAuth authentication flows


## Background and rationale


**Submitted request:**


*In iSHARE, for clear functional reasons, it was decided that authentication should be possible without preregistration at the service provider, since preregistration is done at the scheme owner (later satellite). As a result, every iSHARE authentication flow must support that during the authentication process, identification is done on the basis of certificates, without preregistration. Unfortunately, this is not feasible/scalable, since sending proof of certificates (in the form of client_assertions) is not supported in standard Oauth. In normal OAuth, a preregistration is required, on the basis of which you can retrieve a token with some form of credentials. Therefore, currently, a service provider that wants to support an iSHARE flow must design and implement its own authentication process and cannot use standard open-source OAuth packages. This poses big thresholds for implementing iSHARE and big security risks when implementing iSHARE (as everyone has to reinvent the wheel).*


> [!NOTE]
> For clarity, this RFC is meant for M2M flows and not really addressing the H2M flows.


**Further Background and rationale**


The proposed background does not consider the additional RFCs as extension to the original RFC of oAuth standard. The RFC RFC7521 in its Sections 4.2 and 6.2 exactly describes the `client_assertion` as is described in iSHARE specifications. The RFC further describes the flows in chapter 3 where Figure 2 shows the flows inline with iSHARE specifications. The question is if to support the flows as shown in Figure 1 in the same chapter where the `client_assertion` is created by a (trusted) third party. This document lists down the impact analysis of the same.


Additionally, as mentioned in the Section 7 of the RFC7521, for interoperability there are 2 other RFCs which are profiles based on this specification; namely:
- RFC7522 - SAML 2.0 Assertions
- RFC7523 - JSON Web Tokens (JWTs)


From iSHARE perspective, the profile of JWTs (RFC7523) is relevant and it aligns well with iSHARE specification of client_assertion JWT. Ofcourse, to make it work, iSHARE specifications further define what those attributes could contain.


## Proposed change




### Purpose


**Submitted request:**


*I don't think it is reasonable to expect that the iSHARE adaptation of OAuth will become mainstream, or large enough to support its own open source community. Therefore, iSHARE should think about a RFC to redesign the authentication flow based on:*


*1. keeping it close to iSHARE standards*


*2. restricted to OAuth standards*


**Updated Purpose**


The submitted text assumes that the adoption will not be wide enough, whereas it has not been substantiated enough. iSHARE's specifications as explained above are already quite close to the oAuth standard and the RFC7521 and RFC7523 are already quite widely implemented and available in many libraries across various programming languages. To give some examples:


- **Java Keycloak**:
   - Description: An open-source identity and access management solution that supports OAuth 2.0 and OpenID Connect.
   - Support: Native support for JWT (RFC 7523) is available, and it can be configured to use custom assertion formats.
- **Python Authlib**:
   - Description: A full-featured framework that supports OAuth 1.0 and OAuth 2.0 for client and server.
   - Support: Offers support for JWT and can be extended to implement RFC 7521 and RFC 7523.
- **.NET (C#) Duende Identity Server (formerly IdentityServer4)**:
   - Description: An open-source framework for implementing authentication and authorization in .NET applications.
   - Support: Has extensive support for JWTs and can be configured for RFC 7523 use cases.


For validating certificates, there are native libraries that support validations of x509 and the trust chains they form and most of them natively support specifying Trust Stores which should be based on trusted lists. They also by default perform validity checks including CRL/OCSP checks. Only additional requirement perhaps is to check specific key usage, which should also be natively supported.


Some of the libraries for validating certificates in popular programming languages:


- **Python**
   - cryptography: A comprehensive library for cryptography in Python, supporting X.509 certificate parsing, validation, and handling.
   - pyOpenSSL: A wrapper around a subset of the OpenSSL library, allowing for X.509 certificate handling.
   - M2Crypto: A Python wrapper for OpenSSL, supporting X.509 certificate validation.
- **Java**
   - Bouncy Castle: A widely-used library for cryptographic operations in Java, including X.509 certificate validation.
   - Java Security (JCA/JCE):Java’s built-in security framework provides extensive support for X.509 certificates via the java.security.cert package.
- **C/C++**
   - OpenSSL: A robust, full-featured open-source toolkit for the Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols, supporting X.509 certificates.
   - GnuTLS: A secure communications library implementing SSL, TLS, and DTLS protocols, with support for X.509 certificates.
   - LibreSSL: A version of the TLS/crypto stack forked from OpenSSL, providing X.509 certificate support.


### Description and principles


**Change requested**


Let's first describe what the proposed change means and how it relates to the RFC standard. The proposed change is mainly about ability to get a *Bearer token* issued by a trusted 3rd party like iSHARE Satellite to the Service Consumer, so it can use that to communicate with the Service Provider. The idea is to remove the burden from Service Providers to implement the `/token` endpoint based on iSHARE specifications and instead rely on a trusted 3rd party with the implementation to potentially reduce the risks associated with the custom implementations by Service Providers.
This is primarily based on the finding that many of the oAuth 2.0 libraries natively lack support to RFC7521 and does not support no pre-registrations which is a requirement of iSHARE.


So the idea of this change request is to be as close to the oAuth standard while preserving iSHARE principles.


**Analysis**


Firstly, lets revisit the original RFC of oAuth 2.0, RFC6749 to set the context:


Let's clarify the terminology used in official RFC6749 and that in iSHARE:


| RFC6749 role              | iSHARE role               | Remarks   |
|---------------------------|---------------------------|-----------|
| Resource Owner            | Entitled Party            | The party which grants authorisations to subject           |
| Resource Server           | Service Provider          | The party which has the resource to be provided          |
| Client                    | Service Consumer          | The party which wants the resource(data)          |
| Authorization Server      | Service Provider          | In iSHARE, the only aspect of Authentication is concerned as Authorisation is handled by Authorisation registry which is a separate role.          |


> [!NOTE]
> One of the key differences to keep in mind is oAuth assumes that there is pre established relation between the Resource Server and Authorisation Server (it requires pre-registration). This is different in iSHARE that such a relation existence is not mandatory and still trust can be established.
>Additionally, as per the iSHARE specifications oAuth is only used for Authentication purposes.


There are multiple types of flows supported by oAuth 2.0, namely:
- **Authorization Code Grant**: The authorization code is obtained by using an authorization server as an intermediary between the client and resource owner.
- **Implicit Grant**: In the implicit flow, instead of issuing the client an authorization code, the client is issued an access token directly
- **Resource Owner Password Credentials Grant**: The resource owner password credentials (i.e., username and password) can be used directly as an authorization grant to obtain an access token.
- **Client Credentials Grant**: The client credentials (or other forms of client authentication) can be used as an authorization grant when the authorization scope is limited to the protected resources under the control of the client, or to protected resources previously arranged with the authorization server.
- **Extension Grants**: The client uses an extension grant type by specifying the grant type using an absolute URI (defined by the authorization server) as the value of the "grant_type" parameter of the token endpoint, and by adding any additional parameters necessary.




As you can see from above oAuth protocol is quite a match to iSHARE requirements. Given the flows as explained above, there are potentially 2 flows which would make sense from iSHARE requirements perspective:


**Authorization Code Grant**: This flow is useful when you must authenticate with 3 parties involved during the process and one of the parties is incapable of having PKI certificates or generating client assertions. This is what is needed for Human to Machine flow, where the client authenticates itself at the Authorization Server (Identity Provider) in this case which results in code that can be used eventually by Service Provider to get a token from Identity provider. Do note, the inability here for the client(Service Consumer) to get a token for itself, this is why this works well for user applications where they can use Identity provider to authenticate themselves so that Service Provider can get a token tied to it.




**Client Credential Grant**: This flow allows the Service Consumer to directly fetch a token from the Service Provider, which it can use to access the resource from the Service Provider itself. Typically, a Service Provider has already given a pair of credentials during registration to the Service Consumer (client) which it can use.


However, in iSHARE the requirement is that there is no pre registration required and instead the registration must be looked up from the iSHARE Satellite. This is achieved through use of self generated client credentials by Service Consumer and presenting it to Service Provider. However, why and how can a Service Provider trust self generated credentials? This is achieved by asking the Service Consumer to sign the credentials with its PKI eSEAL certificate so it can be verified that it is indeed the credential of the party claiming it to be its own.  This method of credentials exchange is based on **Self Sovereign Verifiable Credentials** principle.


Since, in iSHARE there are certain specific `roots` of certificates trusted (provided via the trusted list endpoint of satellite), a digital signature using one of the certificates issued by *trusted roots* establishes trust and closes the verification loop. Thus, Service Consumer generates a trustable and verifiable token to authenticate itself to Service Provider.


**iSHARE specifications and oAuth 2.0 Standard**


This works perfectly with Client Credentials grant flow as specified in oAuth 2.0. And this has also been the basis of the  extension RFC7521, where in section 4.2 it describes the client credentials flow following a similar pattern. When the Service Consumer is acting on its own behalf the flow is explained in Section 6.2 as a special case that combines both the authentication and authorization grant usage patterns. In terms of iSHARE, the role of Authorisation Server is therefore integrated into the role of Service Provider as the client/Service Consumer is acting on its own behalf and presenting a token for authentication. Thereby, in iSHARE terms, this authentication mechanism is replaced by PKI eSEAL certificate which is used for digitally signing the `client_assertion`. This follows the principles of Verifiable Credentials, however, at the time of writing this specification originally, Verifiable Credentials as a standard did not exist.


So in summary, the original iSHARE specifications true to its purpose enables decentralization for organizations and enables peer-to-peer transactions and follows Self Sovereignty with Verifiable Credential principles.

**Concluding**

- iSHARE's implementation is in line with the oAuth Client Credentials Grant flow as defined in RFC7521, specifically for clients acting on behalf of itself (chapter 6.2). It uses a JWT client assertion, which is a recognized client assertion type.
- There is a principle difference however and that is that oAuth expects a participant to be (pre) registered by the Service Provider, whereas iSHARE explicitly does not require this.
- The usage of libraries is expected to require iSHARE specific configuration due to this difference. The usage of Saas providers (for instance Auth0) is expected to require specific configuration or even adaptations by the provider.
- The specific iSHARE requirement of not requiring (pre) registration of the client outweighs the downside of not aligning fully with the principle oAuth design.


## Solution direction


Under this RFC051, there is a request from participants to support the concept of Authorisation Server as a separate role so the Authentication (verification of credentials) can happen elsewhere for Service Providers to support it.
To make it easier for Service Providers to work with specifications and reduce entry barriers the authentication can be externalized. This will enable simplified implementations for service providers as they can offload some of the functions to an external party.


Currently, iSHARE Service Providers can utilize an external Secure Token Service/Authorisation Server provider if they wish to. The Service Provider could use a 3rd party who has iSHARE compliant /token endpoint implementation and the Service Provider bilaterally agrees on the service conditions with the 3rd party. Irrespectively, the Service Provider is responsible for implementing the /token endpoint which issues `bearer access tokens` which Service Consumer uses in subsequent service requests. From the perspective of iSHARE framework and that of other iSHARE participants it is still seen as Service Provider implementation and liabilities and risks still remain unchanged. See figure 1 below depicting this option.


**There are several advantages with this approach**:


- No change in liabilities for any participant involved
- The use of external Authorisation Server is abstract to other participants
- It provides more possibilities for Service Providers to cater to extended requirements by reuse of the authorisation server
- Preserves the decentralized nature of interactions between participants
- Authorisation server could bridge between data spaces thereby increasing the interoperability potential
- Allowing to keep neutrality to Satellite role
- Bring it more closure to oAuth 2.0 and perhaps improving the security aspects as same Server/Service could be potentially used by many Service Providers
- In some cases improves privacy as direct link between service consumer and service provider may not be deducible


------


![Authorisation performed by Authorisation Server for Service Provider](res/authSPAuthServer.png)


**Figure 1: Authorisation performed by Authorisation Server for Service Provider**


------


## Externalise Authentication/Define role of Verifier:


For further clarity in the transaction, a new role could be included in iSHARE role model definitions, where Authorisation Server role is defined which then implements the /authorize endpoint specifications to be defined based on oAuth 2.0 specifications while being compatible with iSHARE specifications.
This will enable the flow as described in section 4.1 in RFC7521 (Assertion Created by Third Party), thereby allowing Service Provider to externalize authentication of Service Consumer in a more decentralized way. Since this will be an essential role on which other parties will rely on during a transaction, it must be an iSHARE certified role, which will be subject to governance and service levels as defined for certified roles in the framework. Note, in the context of a data space, it may well be organized that such a 3rd party is a central role within a data space.


> [!NOTE]
> Under RFC040, this role definition is most likely to come anyways in the name of `Verifier` as per the **Verifiable Credentials standard**. If this option is finalized, the same role should support both the oAuth 2.0 as well as Verifier specifications


There are multiple ways in which then this can be achieved:


- **Authorisation Server/STS as an independent role**: Figure 2 below shows how this could work. As you see, this does not change much for Service Provider, as it must still perform the same steps as it was doing to authenticate the Service Consumer. However, some optimisation can be achieved by use of caching, if possible and is within the risk appetite of the Service Provider and/or resource being exchanged. Having said that, it gives flexibility to both Service Consumers and Service Providers in terms of which Authorisation Servers to use.


------


![Authorisation performed by Authorisation Server role](res/auth3rdpartyAuthServer.png)


**Figure 2: Authorisation performed by Authorisation Server role**


------


- **iSHARE Satellite acting as Authorisation Server**: Figure 3 below shows how this could work. As you see, this simplifies a lot of processing for both Service Consumer as well as Service Provider and can further reduce the barrier for entry for them. Here the assumption is that the Service Provider will have implicit trust on its Satellite (where they are registered). However, this approach also comes with following challenges:
   - Satellite responsibilities and liabilities increases:
       - Now it must take the liability on validity of the information at each transaction time on all the additional information(attributes) that are added in the Client Assertion. Ofcourse, when the information is standard, it does not add additional liabilities, however, that could become restrictive from current specifications.
       - Heavy infrastructure capacity is required to ensure that it still functions well within the SLAs with increased use of its endpoints. Specially, for larger data spaces with very active participants the load could be quite high
       - It also will result in more operational costs due to extra responsibilities and heavy infrastructure requirements. Additionally, to limit liabilities it must perform more frequent verification of all parties registered.
       - Any outage will result in severe liabilities as none of the participants will be able to transact in a given data space
       - May result into difficult business model for Satellites
   - Results in more centralisation:
       - Since now there is a central component/role that performs the Authentication of participants in each transaction, it deviates from the principles of decentralization
       - Increases the risk from security perspective as now an single point of failure is introduced
   - Interoperability challenges:
       - Now since the Service Provider depends on a Satellite to provide an assertion of Service Consumer, incase of Service Consumer coming from other data space may not be able to transact. This will be challenging especially when quite a low number of participants of 2 data spaces are infrequently involved


- It would have following advantages on other hand:
   - Simplify Service Provider specifications and unburden them of oAuth server specific implementations
   - Security patches and fixes would be easier and/or faster with broader reach
   - Improved trust in participants registered at the satellite due to frequent checks on their validity of the information
   - In some cases improves the privacy of Service Consumers as Satellite does not know where they are presenting/using this assertion




------
![Authorisation performed by iSHARE Satellite](res/authSatelliteasAuthServer.png)


**Figure 3: Authorisation performed by iSHARE Satellite**


------


## Conclusion and Recommendations:


Based on the above explanations, it clarifies the alignment of iSHARE specifications to oAuth 2.0 standard. Further it explains the reasons for deviations and their advantages and disadvantages.
With the help of flows it is explained what each option could look like. Note, there are more flows possible, however, they pose even higher security risks and/or complexities and have been omitted for brevity.


Based on the explanations above, use of a 3rd party Authorisation Server under Service Provider option is recommended as it still allows to offload the burden to a 3rd party however, it maintains the current specifications and interoperability aspects better then other options. Also keeping it simple from a legal perspective in terms of liabilities and responsibilities. On top of it, there is no change in the framework needed to accommodate that option.

Addressing the concerns raised with RFC:

1. *The usage of libraries is expected to require iSHARE specific configuration due to this difference. The usage of Saas providers (for instance Auth0) is expected to require specific configuration or even adaptations by the provider.*

      a. The oAuth specifications in RFC6749 gives room for various implementations, including extentions. Each party implementing it may have some degree of variation in their implementations. Question about SaaS solutions like Auth0 and Okta supporting the extensions would be simply for these organisations to implement and support it. This is likely possible if their customers demand such implementation. Going in details in terms of how this can be acheived is beyond the scope of this document and process. However, to enable broader support for providing ready to use library/component, we propose following:

      - An open source community project is started under larger OSS foundations like Eclipse, Linux, etc. where a common library/component is developed and maintained by participants of iSHARE as well as any other interesed parties
      - Depending on the tech stack, the work can build upon existing open soruce works available on Git repositories of iSHARE foundation and/or their participants
      - iSHARE Foundation could provide with co-ordination and governance support as necessary to the community
      - In short term, though, this may not result in SaaS providers to directly fully support it, but depending on the customer demands they may in future. 


### Example use cases


Clarify the RFC by describing example use cases in which the change is used in practice.


## Impact on the ecosystem


The following table lists the impact of this RFC on the formal iSHARE roles (excluding the Scheme Owner role).


| Formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Service Consumer          | Yes/no            | Yes/no                                             |
| Service Provider          | Yes/no            | Yes/no                                             |
| Entitled party            | Yes/no            | Yes/no                                             |
| Authorization Registry    | Yes/no            | Yes/no                                             |
| Identity Provider         | Yes/no            | Yes/no                                             |
| Identity Broker           | Yes/no            | Yes/no                                             |
| Data Space Administrator  | Yes/no            | Yes/no                                             |
| Satellite                 | Yes/no            | Yes/no                                             |


## Impact iSHARE Foundation (Scheme Owner)



The developer documentation (part of the iSHARE Framework) will be extended so that it clarifies better on which oAuth specification the iSHARE specifications are based → write this in RFC impact analysis

iSHARE Foundation will create an article on the Knowledge Base (https://trustbok.ishare.eu/) explaining iSHARE's oAuth implementation in more detail. The article will contain a list of known libraries that support the used standards

Another article will be written on the potential use of an Authorisation Server under responsibility of the Service Provider (option 1)


- The [iSHARE Trust Framework](https://framework.ishare.eu)
  - No impact
- The [developer documentation](https://dev.ishare.eu) (as an extension of the iSHARE Trust Framework)
  - The developer documentation (part of the iSHARE Framework) will be extended so that it clarifies better on which oAuth specification the iSHARE specifications are based → write this in RFC impact analysis
- The OpenAPI definitions on [Swaggerhub](https://app.swaggerhub.com/search?owner=iSHARE)
  - No impact
- Example implementation in [Postman Collections](https://dev.ishare.eu/demo-and-testing/postman.html)
  - No impact
- Code that is published on Github:
   - [iSHARE Satellite reference implementation](https://github.com/iSHAREScheme/iSHARESatellite)
     - No impact
   - [eSEAL certificate procurement guide](https://github.com/iSHAREScheme/eSEALsGuide)
     - No impact
   - [iSHARE.NET service consumer core components](https://github.com/iSHAREScheme/iSHARE.NET)
     - No impact
   - [Python iSHARE package](https://github.com/iSHAREScheme/python-ishare)
     - No impact
   - [iSHARE code snippets](https://github.com/iSHAREScheme/code-snippets)
     - No impact
   - [Reference implementation for Authorization Registry](https://github.com/iSHAREScheme/AuthorizationRegistry)
     - No impact
   - [Reference implementation for Service Provider](https://github.com/iSHAREScheme/ServiceProvider)
     - No impact
- The implementation of the iSHARE satellite for iSHARE as the Scheme Owner on https://sat.ishare.eu and https://sat.uat.isharetest.net
  - No impact
- The [public website](https://www.ishare.eu)
  - No impact
- Internal documentation
  - Internal documentation will be updated with background information.
- [Authorization Registry test implementation](https://ar.isharetest.net/)
  - No impact
- The [Conformance Test Tool](https://ctt.isharetest.net/admin/account/login), tests listed on https://ctt.isharetest.net/admin/test-cases
  - No impact
- iSHARE test satellite (used for conformance testing): https://scheme.isharetest.net/
  - No impact
- iSHARE test certificate authority: [EJBCA Public Web](https://ca7.isharetest.net:8442/ejbca/)
  - No impact
- [iSHARE Change Management documentation](https://changes.ishare.eu)
  - No impact
- [iSHARE Trust Body of Knowledge](https://trustbok.ishare.eu/)
  - iSHARE Foundation will create an article on the Knowledge Base (https://trustbok.ishare.eu/) explaining iSHARE's oAuth implementation in more detail. The article will contain a list of known libraries that support the used standards
  - iSHARE Foundation will create an article on the potential use of an Authorisation Server under responsibility of the Service Provider (option 1)



## Implementation


### Release schedule


Describe the foreseen release planning for this RFC. Should it be combined with other RFCs? What is the required release timeframe?


### Communication


Describe required communication topics and means.



