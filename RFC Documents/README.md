# RFC Impact analysis

These pages contain finalised impact analysis for the requested RFCs. Draft impact analysis are available as [merge requests](https://gitlab.com/ishare-foundation/cab/rfc/-/merge\_requests) or [branches](https://gitlab.com/ishare-foundation/cab/rfc/-/branches) on Gitlab.
