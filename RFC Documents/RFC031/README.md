# RFC031: Introduce iSHARE-ID and allow multiple identifiers using DID

| Document property              | Value                 |
| ------------------------------ | --------------------- |
| Issue reference                | #11+                  |
| Document status [draft/final]  | Final                 |

## Background and rationale

Currently iSHARE uses [EORI numbers](https://taxation-customs.ec.europa.eu/customs-4/customs-procedures-import-and-export-0/customs-procedures/economic-operators-registration-and-identification-number-eori_en) as a method for identifying [Participants](https://ishareworks.atlassian.net/wiki/spaces/ETA/pages/2733605361/Framework+and+roles). iSHARE does not provide it's own identifier to participants.

The prescribed use of EORI number has limitations:

- It is not actively used by many organisations (usually only in the context of international trade, when dealing with customs)
- It is only available to European economic operators
- It does not allow data spaces to use their own identification number, while in existing cooperations / industries  an industry-specific standard is usually widely available

This feels like a barrier for participants to join iSHARE, which is why this has led to requests to support additional identifiers then EORI. This RFC responds to these requests.
 
## Proposed change

### Purpose

To reduce the barrier for participants to join iSHARE, by means of allowing them to join using any identifier they already have, there is a need for allowing the usage of other identifiers. This RFC researches options and proposes an implementation for allowing other identifiers to be used.

### Description and principles

At first we have researched candidates that could be an alternative to or be used next to or as an alternative to the EORI-number. 

#### Considered candidates

In our research we have drafted the following list of known identifiers (from a Dutch/international perspective):

**Public sector identifiers**

- Identifiers of legal entities registered in business registers - Chamber of Commerce, VAT, Tax Identification Number, RSIN, NTR, etc.
- ISO 20275: 2017 Entity Legal Form (ELF)
- ISO 17442: 2020 Legal Entity Identifier (LEI)
- ISO 5009: 2021 Official Organizational Roles (OOR)
- Traders Identification Number (TIN)
- Economic Operators Registration Identification Number (EORI)
- The European Unique Identifier (EUID)
- EU VAT Identification Number (IOSS)
- EuroGroups Register (EGR)
- Global Groups Register (GGR)

**Private sector identifiers**

- Global Location Number (GLN)
- ISO 6523:1998 Identification of Organizations Identifications Schemes
- D-U-N-S Number
- PermID
- ISO 9362: 2014 Business Identification Number (BIC)

This list is probably far from complete, as industries often use their own specific identifiers. We have researched two options as a possible alternative to EORI-numbers.

**Option 1: [D-U-N-S Number](https://www.dnb.com/duns-number.html)**

- DUNS number is used worldwide in many industries by many organisations, however market reach and acceptance is still to be ascertained
- This is operated by a private company based in US and is profit oriented. This is against our principle of federated/distributed/open standards based
- There are costs involved for participants to procure this number
- This number is only available for businesses

*Conclusion: not viable*

**Option 2: [GLEI (global Legal Entity Identifier)](https://www.gleif.org/en/about-lei/gleif-management-of-the-global-lei-system)**

- This is an initiative the Group of Twenty (G20) called on the Financial Stability Board (FSB) has endorsed
- Market reach and acceptance is still to be ascertained
- There are costs involved for participants to procure this number
- This number is only available for businesses

*Conclusion: not viable*

The overall conclusion of our market research is that there is no identifier that is commonly used internationally and that has a sufficient reach. The EORI number still seems to be the best fit from that perspective.

#### Solution principles

Considering the requirements of data spaces and the iSHARE ambition to allow data spaces to be interoperable in IAA, the following solution (principles) is considered to be the best fit:

- Each iSHARE Satellite (Data Space Authority) must define which identifier(s) must be used for identification of participants in the data space. *The use of a [DID-method](https://www.w3.org/TR/did-spec-registries/#did-methods) is advised, but self-defined methods are also allowed.*
    - The identification (number/string) must be unique for each participant
    - There will be no predefined list of approved identifiers published by the Scheme Owner
- Each participant of a data space that is connected to the iSHARE network, must be provided with an iSHARE-ID
    - The iSHARE-ID will be provided in the form of a [DID](https://www.w3.org/TR/did-core/)
    - The ID uniquely identifies an iSHARE participant across data spaces
    - The ID must be provided to the participants by the first iSHARE Satellite where a party onboards
    - The iSHARE-ID will be equal to the organization identifier in an attribute of PKI certificate of the participant. This organization identifier has been validated and input by the Certificate Authority (CA) issuing the certificate after following a due diligence process. For example, when Participant is onboarded using and eIDAS certificate, the identifier in the Subject of the eIDAS certificate is in the ASN.OID - "2.5.4.97" or commonly known as OrganizationIdentifier (issued by a Trust Service Provider (TSP)), with the format of this field described in [ETSI EN 319 412-1 V1.5.1, paragraph 5.1.4](https://www.etsi.org/deliver/etsi_en/319400_319499/31941201/01.05.01_60/en_31941201v010501p.pdf).

Using these principles parties can use an unlimited number of identifiers across an unlimited number of data spaces. The common identifier method for all participants all across will be iSHARE-ID at minimum, thereby ensuring interoperability and discoverability. The iSHARE-ID should always be derived from a PKI certificate (currently eIDAS, in the future additionally other such certificates that will be trusted across framework). This also makes sure that this ID is always verifiable programatically which enhances the trust value.

We have considered using the proposed ELSI method ([1](https://www.etsi.org/deliver/etsi_gr/CIM/001_099/018/01.01.01_60/gr_CIM018v010101p.pdf), [2](https://data-spaces-business-alliance.eu/wp-content/uploads/dlm_uploads/Data-Spaces-Business-Alliance-Technical-Convergence-V2.pdf)) instead of iSHARE-ID. This method is however not yet registered and limited to usage in Europe, which is why we decided to use an iSHARE defined DID method instead. Usage of this method is ofcourse not prohibited. The DID-Method Specific Identifier will be equal for both an ELSI method and an iSHARE method.

#### Using DID to store multiple identifiers

Based on the discussions we had in past CAB meetings and evaluations of various options we recommend to use [Decentralized Identifiers (DIDs)](https://www.w3.org/TR/did-core/) as the format for identifiers. This has added advantage of being aligned with other initiative like GAIA-X, IDS, EBSI, etc.

- DID formats have “[methods](https://www.w3.org/TR/did-core/#dfn-did-methods)”, which determine how the identifier must be interpreted. Methods can be used by data spaces to define a type of identification number that will be registered for a participant.
- One method will be defined by the Scheme Owner: this will be the iSHARE-ID method.
- Each satellite must be able to resolve the iSHARE DID method to a DID document. 

An example of a party identifier is:

```json
[
    did:ishare:NTRNL-12345678,
    did:elsi:LEIXG-724500AZSGBRY55MNS59,
    did:web:example.com
    dataspace_selfdefined_identifier:123456
    ...
]
```

In this example:
- DID method "ishare" refers to the method defined by the Scheme Owner as the iSHARE-ID and to be registered as DID method.
- DID method "elsi" refers to the not yet registered ELSI method.
- DID method "web" refers to the registered DID method [web](https://w3c-ccg.github.io/did-method-web/).
- "dataspace_selfdefined_identifier" is an identifier define by a data space and only used in that data space and other data spaces that have decided to trust this identifier.

#### How does this affect interoperability

- Each participant that wishes to operate in multiple data spaces connected to the iSHARE network should at least have an iSHARE-ID.
- Participants in other data spaces can discover this participant by using it's iSHARE-ID.
- Other data spaces can append identifiers to the DID array and thus provide better discoverability in their own data space (if a different ID is predominantly used in this data space).
- By using other DID-methods for participants, interoperability and discoverability can further be enhanced.
- Self defined identifiers are only trusted within a data space that supports these, and in data spaces that explicitly trust these identifiers.

### Example use cases
Consider the following scenario:

- An iSHARE Satellite (Data Space Authority) has selected KvK-number to be accepted as an identifier within it's data space.
- An onboarding procedure has been established by the satellite in which it is determined how proof should be presented for registration of this KvK-number: authentication with eHerkenning level 3+.
- The satellite is part of the iSHARE network.

This impacts the data space in the following way:

- When onboarding a participant (other then Entitled Parties), the participant should
    1. Present an eIDAS certificate, from which the iSHARE-ID will be derived and
    2. Authenticate with eHerkenning level 3+, from which the KvK-number will be copied.
- When a Service Consumer requests a service from a Service Provider
    - The Service Provider can check the adherence of the Service Consumer with the iSHARE Satellite using either the iSHARE-ID, or the KvK-number, or both.
    - The Service Provider can provide delegation evidence using the iSHARE-ID or the KvK-number.

## Impact on the ecosystem

The following table lists the impact of this RFC on the formal iSHARE roles (excluding the Scheme Owner role).

| Formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Service Consumer          | Yes            | Yes                                             |
| Service Provider          | Yes            | Yes                                             |
| Entitled party            | Yes            | Yes                                             |
| Authorization Registry    | Yes            | Yes                                             |
| Identity Provider         | Yes            | Yes                                             |
| Identity Broker           | Yes            | Yes                                             |
| Data Space Administrator  | Yes            | Yes                                             |
| Satellite                 | Yes            | Yes                                             |

## Impact iSHARE Foundation (Scheme Owner)

- The Trust Framework must reflect the changes in this RFC, specifically on 
  - [The page about EORI identification](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70221905/Identification+by+EORI)
  - [The glossary](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222187/Glossary)
  - [The Admission process](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222125/Admission)
  - [The JWT example payload](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70221964/JSON+Web+Token+JWT)
- The developer documentation (as an extension of the iSHARE Trust Framework): [https://dev.ishare.eu/](https://dev.ishare.eu/) will be changed extensively, since the current identification number EORI is used widely. This especially accounts for the /parties and /party endpoints of the iSHARE Satellite.
- Example implementation in [Postman Collections](https://dev.ishare.eu/demo-and-testing/postman.html) must be changed.
- The iSHARE Satellite reference codebase as developed on: [https://github.com/iSHAREScheme/iSHARESatellite](https://github.com/iSHAREScheme/iSHARESatellite) must be changed.
- The implementation of the iSHARE Satellite for iSHARE as the Scheme Owner on [https://sat.ishare.eu](https://sat.ishare.eu) and [https://sat.uat.isharetest.net](https://sat.uat.isharetest.net) must be changed.
- Authorization Registry test implementation: [https://ar.isharetest.net/](https://ar.isharetest.net/) must be changed to accept different identifiers.
- The Conformance Test Tool: [https://ctt.isharetest.net/admin/account/login](https://ctt.isharetest.net/admin/account/login), tests listed on [https://ctt.isharetest.net/admin/test-cases](https://ctt.isharetest.net/admin/test-cases) must be changed.
- iSHARE test satellite (used for conformance testing): [https://scheme.isharetest.net/](https://scheme.isharetest.net/) must be changed.
- iSHARE foundation
  - Will propose ishare as an official DID method

## Implementation

### Release schedule

The impact of this RFC is large and it is hard to provide backwards compatability. An implementation plan must be drafted and discussed with stakeholders.

### Communication

[ ] TODO Describe required communication topics and means.