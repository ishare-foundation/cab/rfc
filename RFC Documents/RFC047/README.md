# RFC047: Capability endpoint improvements

## Background and rationale

The [/capabilities endpoint](https://dev.ishare.eu/common/capabilities.html) is required for every participant that provides services. The endpoint returns iSHARE capabilities (supported versions & optional features) of the iSHARE party. This way it forms a cornerstone in the technical implementation of the iSHARE Framework.

[DSGO](https://afsprakenstelseldsgo.atlassian.net/wiki/spaces/DSGO/overview) is developing a data space for the built environment. The current design of the capabilities object limits its utility within DSGO's data space. Therefore, they recommend modifications for broader generic utility, allowing it to be used in the DSGO and other data spaces.

The [DIL](https://datainlogistics.org/) programme is developing [BDI ("Basis Data Infrastructuur")](https://bdinetwork.org/) and is working on implementation in the logistics domain. As part of the programme, [Portbase](https://portbase.nl/) developed a proof of concept. During the proof of concept several possible improvements of the capabilities endpoint have risen.

During the lifetime of iSHARE, new standards have been released. Particularly [DCAT version 2](https://www.w3.org/TR/vocab-dcat-2/#Class:Data_Service) is a relevant standard, which introduced the concept of a Data Service, with a comparable scope as the iSHARE /capabilites endpoint.

## Proposed change

### Purpose

This RFC intends to improve the specification of the capabilities endpoint (more specifically the capabilities_info object) to foster adaptability for data spaces and to improve alignment with new standardization.

### Description and principles

The following adaptations are foreseen:

- Loosely base the structure of a capability on the [DCAT version 3 class Data Service](https://www.w3.org/TR/vocab-dcat-3/#Class:Data_Service), building on the work that has been done by DSGO on defining the [Data_service_info object](https://afsprakenstelseldsgo.atlassian.net/wiki/spaces/DSGO/pages/245538140/Data+service+specification#Data_service_info-object).
- Include a property to refer to a description of the endpoint in OpenAPI format, comparable with the [endpoint_description property](https://www.w3.org/TR/vocab-dcat-3/#Property:data_service_endpoint_description) in the DCAT Data Service class.
- Restructure the capabilities_info token to a flatter structure by removing the version hierarchy.
- Allow the listing of data space specific roles besides iSHARE roles.
- Rename features to data services, thereby relating to the DCAT Data Service class.

## Impact on the ecosystem

The following table lists the impact of this RFC on the formal iSHARE roles (excluding the Scheme Owner role).

| Formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Service Consumer          | Yes             | No                                             |
| Service Provider          | Yes             | No                                             |
| Entitled party            | Yes             | No                                             |
| Authorization Registry    | Yes             | No                                             |
| Identity Provider         | Yes             | No                                             |
| Identity Broker           | Yes             | No                                             |
| Data Space Administrator  | Yes             | No                                             |
| Satellite                 | Yes             | No                                             |

## Impact iSHARE Foundation (Scheme Owner)

- The [iSHARE Trust Framework](https://framework.ishare.eu): no impact
- The [developer documentation](https://dev.ishare.eu) (as an extension of the iSHARE Trust Framework): impact on the description of the [/capabilities endpoint](https://dev.ishare.eu/common/capabilities.html) for all roles
- The OpenAPI definitions on [Swaggerhub](https://app.swaggerhub.com/search?owner=iSHARE): impact on the description of the /capabilities endpoint for all roles
- Example implementation in [Postman Collections](https://dev.ishare.eu/demo-and-testing/postman.html): these implementations must be updated to implement the new /capabilities endpoint
- Code that is published on Github:
  - [iSHARE Satellite reference implementation](https://github.com/iSHAREScheme/iSHARESatellite): yes, implement new endpoint specifications
  - [eSEAL certificate procurement guide](https://github.com/iSHAREScheme/eSEALsGuide): no impact
  - [iSHARE.NET service consumer core components](https://github.com/iSHAREScheme/iSHARE.NET): no impact
  - [Python iSHARE package](https://github.com/iSHAREScheme/python-ishare): yes, implement new endpoint specifications
  - [iSHARE code snippets](https://github.com/iSHAREScheme/code-snippets): yes, implement new endpoint specifications
  - [Reference implementation for Authorization Registry](https://github.com/iSHAREScheme/AuthorizationRegistry): yes, implement new endpoint specifications
  - [Reference implementation for Service Provider](https://github.com/iSHAREScheme/ServiceProvider): yes, implement new endpoint specifications
- The implementation of the iSHARE satellite for iSHARE as the Scheme Owner on https://sat.ishare.eu and https://sat.uat.isharetest.net: yes, must be updated with a new version of the iSHARE Satellite, after updating that
- The [public website](https://www.ishare.eu): yes, specifically this page: https://ishare.eu/ishare-capabilities-end-point-explained/
- Internal documentation: possibly
- [Authorization Registry test implementation](https://ar.isharetest.net/): yes, must be updated with a new version of the AR, after updating that
- The [Conformance Test Tool](https://ctt.isharetest.net/admin/account/login), tests listed on https://ctt.isharetest.net/admin/test-cases
- iSHARE test satellite (used for conformance testing): https://scheme.isharetest.net/: yes, must be updated with a new version of the iSHARE Satellite, after updating that
- iSHARE test certificate authority: [EJBCA Public Web](https://ca7.isharetest.net:8442/ejbca/): no impact
- [iSHARE Change Management documentation](https://changes.ishare.eu): no impact

## Implementation

### Release schedule

Describe the foreseen release planning for this RFC. Should it be combined with other RFCs? What is the required release timeframe?

### Communication

Describe required communication topics and means.