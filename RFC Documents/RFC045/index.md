# RFC045: Introduce zero-trust network

## Background and rationale

One of iSHARE's main principles is that organisations can become participant of a data space based on the iSHARE framework. Once an organisation is a participant, organisations are trusted by other data spaces that are also based on the iSHARE framework. Data spaces can optionally require additional trust requirements for their participants.

Members of a data space can communicate with participants of all data spaces within the iSHARE network, based on the trust that is provided by the iSHARE Trust Framework. To support interoperability, the iSHARE Satellites are currently connected using a distributed ledger. This leads to better discoverability of participants and easier onboarding for participants in multiple data spaces. This technology however, is not part of the iSHARE Trust Framework itself.

Closely related to this RFC are the following RFCs:
- RFC044: #2+
- RFC040: #5+

## Proposed change

### Purpose

This RFC started with the requirement to accomodate an approach where the trust perimeter is less strict: a perimeterless approach, i.e. a zero-trust network. Organisations can interact with other organisations based on a more specific level of trust. Level of trust requirements could for instance be membership of an iSHARE based data space, membership of a specific data space, or other credentials (such as a certain certification, or a recommendation by a data space member).

In principle, iSHARE already accomodates this approach. With this RFC however, we want to further clarify how to implement this scenario with iSHARE and identify possible improvements in the iSHARE Trust Framework. This RFC supports the goals of iSHARE and complies with all [guiding principles](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222189/Guiding+principles), strenthening international orientation (principle 6).

### Description and principles

To accomodate the idea of a zero-trust network, Verifiable Credentials **may** be used. This change is covered in RFC040: #5+. In general, the following logic will take place when **a Service Consumer requests a service from a Service Provider**, based on a Verifiable Credentials approach.

- The Service Consumer provides it's identity (either via the existing way, or via a Verifiable Credential (see RFC040: #5+))
- The Service Provider decides if there is a need for additional credentials and if so, requests this from the Service Consumer (using a [Verifiable Presentation Request](https://w3c-ccg.github.io/vp-request-spec/))
- The Service Consumer provides the additional credentials (using Verifiable Credentials)
- The Service Provider decides if the service can be provided
- The Service Provider fetches the required delegation evidence from the Autorization Registry, or the Service Consumer provides the delegation evidence
- If all required delegation evidence is present, the service is provided

Other implementation methods then Verifiable Credentials are also able to solve this. Irrespective of which method is chosen, the iSHARE Trust Framework provides legal coverage.

Existing iSHARE data spaces are not affected by this change, which makes this change backwards compatible.

### Example use cases

> #### Example: nested data spaces
> This RFC supports the idea of data space *branches*. Let's take the following example.
> - Consider the data space `Inland Shipping NL`, where
> - `Inland Shipping NL` is a branche of `Shipping NL`, and
> - `Shipping NL` is a branche of `Logistics NL`
>
> ![Example of nested data spaces diagram](./Example%20of%20nested%20data%20spaces.png)
> 
> A Service Provider (member of `Inland Shipping NL`) receives a request to consume a service by a Service Consumer. The Service Provider could decide *on behalf of the Entitled Party* to deliver it's service with a following logic:
> - If the Service Consumer is also member of `Inland Shipping NL` (Service Consumer A) AND the correct Authorizations from the Entitled Party are present, the service can be delivered
> - If the Service Consumer is not member of `Inland Shipping NL`, but is member of `Shipping NL` (Service Consumer B), the Service Provider requires extra credentials AND requires the correct Authorizations, after which the service can be delivered
> - If the Service Consumer is not member of `Inland Shipping NL`, but is member of `Logistics NL` (Service Consumer C), the Service Provider requires even more credentials AND required the correct Authorizations, after which the service can be delivered

Of course this is just an example. The 'nesting' of data spaces is not required. A participant can be member of any number of data spaces within the iSHARE network and present proof for validations to the Satellite as and when necessary. These data spaces don't require any relationship other then that they are both connected to the iSHARE network.

## Impact on the ecosystem

- [ ] TODO: assess roles.

The following table lists the impact of this RFC on the formal iSHARE roles (excluding the Scheme Owner role).

| Formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Service Consumer          | Yes/no            | Yes/no                                             |
| Service Provider          | Yes/no            | Yes/no                                             |
| Entitled party            | Yes/no            | Yes/no                                             |
| Authorization Registry    | Yes/no            | Yes/no                                             |
| Identity Provider         | Yes/no            | Yes/no                                             |
| Identity Broker           | Yes/no            | Yes/no                                             |
| Data Space Administrator  | Yes/no            | Yes/no                                             |
| Satellite                 | Yes/no            | Yes/no                                             |

## Impact iSHARE Foundation (Scheme Owner)

A preliminary impact analysis is that the following assets are impacted:

- The iSHARE Trust Framework: [https://ishareworks.atlassian.net/wiki/spaces/IS/](https://ishareworks.atlassian.net/wiki/spaces/IS/): prepares it for the approach that is described in this RFC. Other assets are impacted with the implementation of other RFCs (RFC040 and RFC044).
- The public website [https://www.ishare.eu](https://www.ishare.eu), for providing extra guidance on this approach.
- The developer documentation (as an extension of the iSHARE Trust Framework): [https://dev.ishare.eu/](https://dev.ishare.eu/). We don't expect any changes in for example API endpoints, but clarifying texts might be changed.
- The external support documentation on [https://support.ishare.eu](https://support.ishare.eu) needs to be updated.

During implementation it might become clear that other assets are impacted as well.

## Implementation

### Release schedule

- [ ] TODO: Describe the foreseen release planning for this RFC. Should it be combined with other RFCs? What is the required release timeframe?

### Communication

- [ ] TODO: Describe required communication topics and means.