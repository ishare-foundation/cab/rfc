# RFC034: Define standard delegation policy creation request endpoint for AR

| Document property              | Value                 |
| ------------------------------ | --------------------- |
| Issue reference                | #9+                   |
| Document status [draft/final]  | Draft                 |

## Background and rationale

Currently iSHARE certified Authorization Registries (see [functional requirements](https://framework.ishare.eu/is/functional-requirements-per-role#Functionalrequirementsperrole-AuthorizationRegistryAuthorizationRegistry) or [general information](https://ishare.eu/about-ishare/authorization-registry/)):

- Can hold information on delegations to other entities by Entitled Parties;
- Have a process in place allowing for the registration, update and revocation of delegations;
- Can check, on the basis of this information, whether a legal entity is authorized to take delivery of a service;
- Can, after a request in the form of a "delegation mask" is presented, confirm whether this is the case by either:
  - Providing delegation evidence to the Service Consumer, which then can present the evidence to the Service Provider;
  - Providing delegation evidence to the Service Provider.

There is no prescribed API endpoint to request creating, updating and deleting delegations. An Authorization Registry provider is free to design the process for registration, update and revocation of delegations (for instance by providing a user interface, an API, a manual process, connections with existing software, or any other method).

iSHARE currently only defines the /delegation endpoint for an Authorization Registry. The basic structure is:

- [/delegation endpoint](https://dev.ishare.eu/delegation/endpoint.html)
  - [Request](https://dev.ishare.eu/delegation/endpoint.html#request) contains a [delegationRequest (Delegation Mask)](https://dev.ishare.eu/delegation/delegation-request.html)
    - Which contains [policySets](https://dev.ishare.eu/delegation/policy-sets.html#refpolicysets)
      - Which contain [policies](https://dev.ishare.eu/delegation/policy-sets.html#refpolicies)
  - [Response](https://dev.ishare.eu/delegation/endpoint.html#refdelegationresponseexample) contains a JWT delegation_token, containing [delegation evidence](https://dev.ishare.eu/delegation/delegation-evidence.html)
    - Which contains [policySets](https://dev.ishare.eu/delegation/policy-sets.html#refpolicysets)
      - Which contain [policies](https://dev.ishare.eu/delegation/policy-sets.html#refpolicies)

## Proposed change

### Purpose

This RFC aims to stimulate the registration of fine grained authorizations by creating the possibility for a machine-to-machine delegation policy request that is automatically approved based on a ruleset provided by an entitled party.

### Description and principles

As an Entitled Party I want to delegate the right to request (to create) delegation policies to a delegated party. I provide the Authorization Registry with a ruleset on which these requests are evaluated. The delegated party can then request the creation of delegation policies on behalf of me, which are evaluated on the ruleset that I have provided and then automatically added as delegation policies.

Principles:

- The party to which the right is delegated is called the “Delegation Policy Requestor”.
- The ruleset used to delegate the right to create and manage delegations on which delegation policy requests are evaluated, is called “authorisation rules”.
- The “Delegation Policy Requestor” can request delegations using an API offered by the Authorization Registry.
- The “Delegation Policy Requestor” can only request delegations at the Authorization Registry at which the authorisation rules are stored (i.e. the Authorization Registry of the Entitled Party).
- The requests may be automatically (dis)approved based on the authorisation rules that are defined by the Entitled Party.
- A wildcard (“*”) authorisation rule, essentially allowing the “Delegation Policy Requestor” to request and manage all of the Entitled Party’s delegations, is not allowed.
- Authorization Registries should follow principles on overlapping policies (see [Authorisation Rules specification](<./Authorisation Rules specification.md>)).

Note that it remains possible to create, update and delete delegations by the Entitled Party itself, making this change fully backwards compatible.

> **_NOTE:_**  For ease of reading, this RFC assumes that delegation policies are stored and managed as such, however this is not a requirement. It could be that an Authorization Registry uses a different method to store delegations (not with policies, but for example using business rules or transaction data). Upon receival of a delegation evidence request the delegation evidence is produced and return. This RFC requires a process in reverse order: a delegation policy request (having a similar structure as delegation evidence) is presented, which must be translated back into the chosen method for storing delegations.

> **_NOTE:_**  The implementation of the described endpoint by the Authorization Registries is mandatory from the iSHARE Framework version in which it is introduced (expected in 2.1), though not mandatory for older versions.

### Example use cases

By implementing the changes in this RFC multiple scenarios will benefit from this. These are best explained using examples. This first example use case describes a situation where an entitled party uses a service provided by a third party. The entitled party registers a generic delegation at his Authorization Registry, setting the boundaries (rules) for delegations that are provisioned from a software solution (for instance an eCMR) that already contains fine grained delegations. The delegations are proactively provisioned from the 3rd party software/service provider, after which the service can be consumed based on these delegations.

In this example we use the following entities (all of them are iSHARE participants):

- `[Banana & co]`: the Entitled Party
- `[eCMR]`: the 3rd party software/service provider used by the Entitled Party
- `[Authorization Registry]`: the Authorization registry
- `[Warehouse 13]`: a Service Provider
- `[ABC Trucking]`: a Service Consumer (acting as a Delegation Policy Requestor)

![Delegations creation flow](./Delegation%20creation%20flow.png)

1. The Entitled Party (`Banana & co`) creates the authorisation rules at Authorization Registry (using an interface provided by the Authorization Registry), determining the scope of delegations that can be created based on the authorisation rules. The rules could for instance contain a list of containers, or a timeframe (all containers in the coming month) and the Service Provider (`Warehouse 13`) where the data is stored.
2. The Entitled Party (`Banana & co`) requests the 3rd party software/service provider (`eCMR`) to provision the Authorization Registry with delegation policies.
3. The 3rd party software/service provider (`eCMR`) requests the creation of fine-grained delegations-policies at the Authorization Registry, based on the authorisation rules and the existing knowledge of fine-grained within the 3rd party software/service provider.
4. The Authorization Registry evaluates the request for creation of the delegation policies with the authorisation rules. If the required delegation falls within the authorisation rules, the delegation is created.
5. _This step is outside the scope of iSHARE._ The Entitled Party requests a service from a party (`Banana & co`) (requires ABC Trucking to pick up a container). The party (`ABC Trucking`) requires data that belongs to the Entitled Party (`Banana & co`) from a Service Provider (`Warehouse 13`) and will therefore act as a Service Consumer. It needs a delegation in order to access this data at the Service Provider (`Warehouse 13`) where the data is stored.
6. The Service Consumer (`ABC Trucking`) requests delegation evidence from the Authorization Registry. The delegation evidence is provided based on the fine grained delegation policies that are stored in the Authorization Registry.
7. With the delegation evidence the Service Consumer (`ABC Trucking`) can request data from the Service Provider (`Warehouse 13`).
8. _This step is outside the scope of iSHARE._ The requested service from the Service Consumer (`ABC Trucking`) is delivered to the Entitled Party (`Banana & co`).

**Other examples** for using the proposed changes in this RFC are:

- An entitled party could delegate the management of delegation-policies to the Service Consumer. The Service Consumer requests the creation of a delegation policy based on the authorisation rules just before accessing a service provided by the Service Provider.
- A party could request a delegation from the Entitled Party by requesting this delegation at the Authorization Registry. The Authorization Registry informs the Entitled Party that a certain delegation is requested, after which the Entitled Party can approve this. This is depicted in the diagram below.

![Delegations creation flow alternative](./Delegation%20creation%20flow%202.png)

## Impact on the ecosystem

The following table lists the impact of this RFC on the formal iSHARE roles (excluding the Scheme Owner role).

| Formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Service Consumer          | Possibly, when it wants to request policy creation | Possibly, when it wants to request policy creation |
| Service Provider          | No                | No                                                 |
| Entitled party            | No                | Yes, create authorisation rules, allow fine grained authorizations to be automatically managed |
| Authorization Registry<sup>1</sup>    | Yes, provide a way to manage authorisation rules and provide an API endpoint for delegation policy request. | Yes, provide operational and functional process to support this as well define the legal boundaries on policies created based on rule sets, if the Authorization Registry chooses to implement the endpoint. |
| Identity Provider         | No                | No                                                 |
| Identity Broker           | No                | No                                                 |
| Data Space Administrator  | No                | No                                                 |
| Satellite                 | No                | No                                                 |

<sup>1</sup> This change is fully backwards compatible with current implementations of Authorization Registries. Current delegations are therefore not affected.

This RFC introduces a non-formal role of a party called “Delegation Policy Requestor”. Any party can fulfill this role, as long as this party is enrolled as a participant in the data space and can therefore be identified when registering the authorisation rules.

| Non-formal role               | Technical impact  | Business / legal / functional / operational impact |
| ------------------------- | ----------------- | -------------------------------------------------- |
| Delegation Policy Requestor          | Yes, implement AR delegation policy request API | Yes, support Entitled Party in providing fine grained authorizations |

## Impact iSHARE Foundation (Scheme Owner)

### iSHARE Trust Framework

The RFC will require changes in the iSHARE Trust Framework (as published [here](https://framework.ishare.eu/)). The following changes are foreseen:

- A new basic data [license](https://framework.ishare.eu/is/licenses) will be defined, which forms the legal basis for requesting the creation of delegation policies. First draft:

```text
9996 Request the creation of delegations on behalf of the Entitled Party, bound to defined authorisation rules, liability lies with Delegation Policy Requestor.
9997 Request the creation of delegations on behalf of the Entitled Party, bound to defined authorisation rules, liability lies with Entitled Party.
9998 Request the creation of delegations on behalf of the Entitled Party, bound to defined authorisation rules, liability is subject to conditions or bilateral agreements.
```

- This license may not be sufficient in all cases and participants are expected to add more definitions of licenses to be included in the licenses list and this way contribute to iSHARE framework.
- In the [structure of delegation evidence](https://framework.ishare.eu/is/structure-of-delegation-evidence) under “Policy” the definition of “type” should indicate that the use of “ISHARE.DELEGATION” is reserved.
- The [functional description per role](https://framework.ishare.eu/is/functional-requirements-per-role) (paragraph Authorization Registry) must be amended.
- [Secondary use cases](https://framework.ishare.eu/is/secondary-use-cases) must be amended.

Since changes are required in the iSHARE Trust Framework this RFC requires a new version of the iSHARE Trust Framework to be drafted, discussed, approved and published.

### Technical documentation

Changes in the technical documentation (https://dev.ishare.eu/) are required, specifically in the Delegations section. The following changes are foreseen:

- The specifications under AUTHORISATION REGISTRY ROLE on https://dev.ishare.eu/ will be appended with a specification for the “Policy Creation Request Endpoint”. For the purpose of this RFC the API requirements are specified as OpenAPI 3 specifications and will form an integral part of the technical requirements to an iSHARE certified Authorization Registry. The proposed API specification is hosted [on Swaggerhub here](https://app.swaggerhub.com/apis/GERARDHUISINTVELD_1/policy-management_api/2.1). The structure of the delegationPolicyRequestToken is defined in the [Policy request endpoint](./Policy%20request%20endpoint.md) section of the developer portal.
- Under the REFERENCE section a page will be created holding requirements to the authorisation rules. A first version of the intended page is provided in [Authorisation Rules specification](./Authorisation%20Rules%20specification.md).

### Conformance test tool

The conformance test tool must be amended with testing of the newly API endpoint. This is a test that is relevant for testing Authorization Registries. Detailed test cases will be defined and shared before they are implemented in CTT.

### Other documentation

Other documentation to be changed:

- https://ishare.eu/about-ishare/roles/
- https://ishare.eu/about-ishare/roles/roles-breakdown/
- https://ishare.eu/about-ishare/authorization-registry/
- In reference implementations of an Authorization Registry it would make sense to make a note about the new requirement.

## Implementation

### Release schedule

To be decided after discussion in the Change Advisory Board.

### Communication

To be defined.
