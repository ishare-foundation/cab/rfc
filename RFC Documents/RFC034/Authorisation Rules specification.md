# Authorisation Rules

| Property                       | Value                 |
| ------------------------------ | --------------------- |
| Issue reference                | #9+                   |
| Document status [draft/final]  | Draft                 |

> **_NOTE:_** This page will become part of https://dev.ishare.eu/.

iSHARE certified Authorization Registries should support the creation of authorisation rules if they choose to implement the policy request endpoint. Authorisation Rules are created by an Entitled Party and contain rules based upon which incoming delegation requests (using the policy creation endpoint /policies) are evaluated and automatically created or refused.

There are no technical specifications on how the Entitled Party should be provided with the possibility of managing authorisation rules. However the following principles should be followed:

- The authorisation rules must use a data license (9998 + additional licenses as applicable) to limit liability on automatically created policies.
- The authorisation rules may use ISHARE.DELEGATION as a resource type.
- The authorisation rules may implement the iSHARE concept of actions that can be performed on the delegations. Action names here are not prescribed, but as a best practice we suggest to use "ISHARE.CREATE", "ISHARE.READ", "ISHARE.UPDATE" and "ISHARE.DELETE" to improve interoperability between Authorization Registries.
- The authorisation rules must be limited with rules that work the same as described in https://framework.ishare.eu/is/structure-of-delegation-evidence. An extra requirement for these authorisation rules is that at least one rule limiting the scope of kind of delegation to be created via this mechanism should be present, preventing “*” authorisation rules.

The Authorization Registry should provide clear information on how the authorisation rules are processed if they overlap. Refer to the guidance section below for further information.

> ## Guidance
>
> The following is not not part of the specifications, but provided as guidance for implementation.
>
> ### Logic when requesting the creation of delegation policies
>
> The Authorization Registry should contain logic for handling delegation policy requests when there are overlapping authorisation rules in place.
>  
> An example of a principle that could be followed is:
>
> - If multiple authorisation rules are present, the most recently added authorisation rules takes precedence over older.
>
> ### Logic when requesting delegation evidence
>
> An Authorization Registry should contain logic for handling delegation evidence requests when there are overlapping delegation policies in place. Since the creation of delegation policies can be automated, it becomes more likely that overlapping policies will occur.
>
> Example of delegation policies can be considered:
>
> - Directly provided by the Entitled Party (direct delegation policies)
> - Provided based on authorisation rules (indirect delegation policies)
>
> Example of principles that could be followed are:
>
> - Directly provided policies take precedence over policies created based on authorisation rules.
> - If multiple delegation policies are present, the most recently added delegation policies takes precedence over older.
> - Within a policy rules should be evaluated in a deny-override manner, allowing a Permit only if all of the rule elements evaluate to Permit.
