# RFC042

| Document property              | Value |
| ------------------------------ | ----- |
| Issue reference                | #1+   |
| Document status \[draft/final] | Draft |

## Background and rationale

With [RFC028](../../RFC%20Documents/RFC028/) iSHARE has move to a federated model. This has led to changes in terminology being used for roles throughout the iSHARE Trust Framework and other iSHARE documentation or assets. Additionally, other initiatives like [GAIA-X](https://gaia-x.eu/) and [IDSA](https://internationaldataspaces.org/) and new regulations like the [Data Governance Act](https://digital-strategy.ec.europa.eu/en/policies/data-governance-act) have come up that have introduced terminology as well.

## Proposed change

### Purpose

This RFC aims to bring clearance to the terminology used for [roles in iSHARE](https://framework.ishare.eu/main-aspects-of-the-ishare-trust-framework/framework-and-roles) and provide clarity in aligning the terminology with other initiatives and regulations. It proposes the renaming of one iSHARE role and proposes changes to align and clarify documentation.

### Description and principles

The roles will be named as follows.

#### Adhering roles

| Current name     | New name         | Other names currently in use for this role | In GAIA-X | In IDSA | In Data Governance Act                                                                                                                                   |
| ---------------- | ---------------- | ------------------------------------------ | --------- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Service Consumer | Service Consumer | Data Consumer                              | TBD       | TBD     | Can be a _data holder_ and a _data user_                                                                                                                 |
| Service Provider | Service Provider | Data Provider, Data Node                   | TBD       | TBD     | Can be a _data holder_ and a _data user_                                                                                                                 |
| Entitled Party   | Entitled Party   | Data Owner, Data Holder                    | TBD       | TBD     | The DGA uses this role combined with either Service Consumer or Service Provider (depending on which one provides the data) and calls this _data holder_ |

#### Certified roles

| Current name           | New name               | Other names currently in use for this role | In GAIA-X | In IDSA | In Data Governance Act |
| ---------------------- | ---------------------- | ------------------------------------------ | --------- | ------- | ---------------------- |
| Identity Provider      | Identity Provider      | --                                         | TBD       | TBD     | TBD                    |
| Identity Broker        | Identity Broker        | --                                         | TBD       | TBD     | TBD                    |
| Authorisation Registry | Authorisation Registry | --                                         | TBD       | TBD     | TBD                    |

#### Other roles

| Current name         | New name                     | Other names currently in use for this role | In GAIA-X | In IDSA | In Data Governance Act |
| -------------------- | ---------------------------- | ------------------------------------------ | --------- | ------- | ---------------------- |
| Scheme Owner         | Scheme owner                 | --                                         | TBD       | TBD     | TBD                    |
| Scheme Administrator | **Data Space Administrator** |                                            | TBD       | TBD     | TBD                    |
| Satellite            | **Data Space Authority**     | Data Space Coordinator, Scheme Satellite   | TBD       | TBD     | TBD                    |

## Impact on the ecosystem

The RFC does not impact the current ecosystem functionally or technically.

## Impact iSHARE Foundation (Scheme Owner)

iSHARE Foundation will review all current assets and work on consistent use of roles as defined in this RFC.

* The iSHARE Trust Framework: [https://framework.ishare.eu/](https://framework.ishare.eu/)
* The developer documentation (as an extension of the iSHARE Trust Framework): [https://dev.ishare.eu/](https://dev.ishare.eu/)
* Example implementation in [Postman Collections](https://dev.ishare.eu/demo-and-testing/postman.html).
* The iSHARE Satellite codebase as developed on: [https://github.com/iSHAREScheme/iSHARESatellite](https://github.com/iSHAREScheme/iSHARESatellite)
* The implementation of the iSHARE satellite for iSHARE as the scheme owner on [https://sat.ishare.eu](https://sat.ishare.eu) and [https://sat.uat.isharetest.net](https://sat.uat.isharetest.net)
* The public website [https://www.ishare.eu](https://www.ishare.eu)
* iSHARE Community forum: [https://forum.ishare.eu/](https://forum.ishare.eu/)
* Internal documentation: [https://drive.google.com/drive/](https://drive.google.com/drive/)
* Authorization Registry test implementation: [https://ar.isharetest.net/](https://ar.isharetest.net/)
* The Conformance Test Tool: [https://ctt.isharetest.net/admin/account/login](https://ctt.isharetest.net/admin/account/login), tests listed on [https://ctt.isharetest.net/admin/test-cases](https://ctt.isharetest.net/admin/test-cases)
* iSHARE test satellite (used for conformance testing): [https://scheme.isharetest.net/](https://scheme.isharetest.net/)
* iSHARE test certificate authority: [EJBCA Public Web (isharetest.net)](https://ca7.isharetest.net:8442/ejbca/)

## Implementation

### Release schedule

The expected implementation time is around 4-6 weeks after deciding on the implementation of this RFC.

### Communication

The implementation of this RFC will be actively communicated with the community.
